# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'alumnos.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import MySQLdb as mdb

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(400, 300)
        MainWindow.setMinimumSize(QtCore.QSize(400, 300))
        MainWindow.setMaximumSize(QtCore.QSize(400, 300))
        font = QtGui.QFont()
        font.setFamily("Segoe UI Light")
        font.setPointSize(10)
        MainWindow.setFont(font)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.Boton = QtWidgets.QPushButton(self.centralwidget)
        self.Boton.setObjectName("Boton")
        self.gridLayout_2.addWidget(self.Boton, 1, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.lineEdit_ap_pa = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_ap_pa.setObjectName("lineEdit_ap_pa")
        self.gridLayout.addWidget(self.lineEdit_ap_pa, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.lineEdit_ap_ma = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_ap_ma.setObjectName("lineEdit_ap_ma")
        self.gridLayout.addWidget(self.lineEdit_ap_ma, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.lineEdit_nombre = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_nombre.setObjectName("lineEdit_nombre")
        self.gridLayout.addWidget(self.lineEdit_nombre, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.Boton.clicked.connect(self.insertarData)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.Boton.setText(_translate("MainWindow", "Hacer Registro"))
        self.label.setText(_translate("MainWindow", "Apellido Paterno:"))
        self.label_2.setText(_translate("MainWindow", "Apellido Materno:"))
        self.label_3.setText(_translate("MainWindow", "Nombre(s):"))

    def insertarData(self):
        conexion = mdb.connect('localhost', 'root', '', 'pyqt5')
        with conexion:
            cursor = conexion.cursor()
            cursor.execute("INSERT INTO alumnos(apellido_paterno, apellido_materno, nombre)"
                            "VALUES('%s', '%s', '%s')" % (''.join(self.lineEdit_ap_pa.text()),
                                                      ''.join(self.lineEdit_ap_ma.text()),
                                                      ''.join(self.lineEdit_nombre.text())))

            QtWidgets.QMessageBox.about(MainWindow, "Exito", "Si se pudo!")
            self.lineEdit_ap_pa.setText('')
            self.lineEdit_ap_ma.setText('')
            self.lineEdit_nombre.setText('')
            

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

