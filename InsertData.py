from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QLineEdit, QPushButton, QMessageBox
import sys
import MySQLdb as mdb

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = "PyQt5 Insert Data"
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.InitWindow()

    def InitWindow(self):

        self.lineedit1 = QLineEdit(self)
        self.lineedit1.setPlaceholderText('Introduce tu nombre')
        self.lineedit1.setGeometry(200,100,200,30)

        self.lineedit2 = QLineEdit(self)
        self.lineedit2.setPlaceholderText('Introduce tu email')
        self.lineedit2.setGeometry(200,150,200,30)

        self.lineedit3 = QLineEdit(self)
        self.lineedit3.setPlaceholderText('Introduce tu Telefono')
        self.lineedit3.setGeometry(200,200,200,30)

        self.button = QPushButton('Insertar Registro', self)
        self.button.setGeometry(200,250,150,30)
        self.button.clicked.connect(self.InsertData)

        self.setWindowTitle(self.title)
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.show()

    def InsertData(self):
        conexion = mdb.connect('localhost', 'root', '', 'pyqt5')
        with conexion:
            cursor = conexion.cursor()
            cursor.execute("INSERT INTO data(name, email, phone)"
                            "VALUES('%s', '%s', '%s')" % (''.join(self.lineedit1.text()),
                                                      ''.join(self.lineedit2.text()),
                                                      ''.join(self.lineedit3.text())))

            QMessageBox.about(self, 'Conexion', 'Datos insertados')
            self.close()



App = QApplication(sys.argv)
window = Window()
sys.exit(App.exec())